<?php

    // Paramètres de connection à MySQL
    $host     = "localhost";
    $user     = "root";
    $password = "";

    // Noms de la base et de la table à utiliser
    $database = "minichat";
    $table    = "messages";

    // Nombre de messages par page
    $count    = 6;
